//
//  PhotoEditor+Controls.swift
//  Pods
//
//  Created by Mohamed Hamed on 6/16/17.
//
//

import Foundation
import UIKit

// MARK: - Control
public enum control {
    case crop
    case sticker
    case draw
    case text
    case save
    case share
    case clear
}

extension PhotoEditorViewController {

    //MARK: Top Toolbar

    @IBAction func cancelButtonTapped(_ sender: Any) {
        editButtonStackView.isHidden = false
        photoEditorDelegate?.canceledEditing()
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func cropButtonTapped(_ sender: UIButton) {
        let controller = CropViewController()
        editButtonStackView.isHidden = true
        controller.delegate = self
        controller.image = image
        cancelButton.isHidden = false
        doneButton.isHidden = false
        saveButton.isHidden = true
        let navigationController = UINavigationController(rootViewController: controller)
        navigationController.modalPresentationStyle = .overFullScreen
        present(navigationController, animated: true, completion: nil)
    }

    @IBAction func stickersButtonTapped(_ sender: Any) {
        addStickersViewController()
    }

    @IBAction func drawButtonTapped(_ sender: Any) {
        editButtonStackView.isHidden = true
        isDrawing = true
        canvasImageView.isUserInteractionEnabled = false
        cancelButton.isHidden = false
        doneButton.isHidden = false
        saveButton.isHidden = true
        colorPickerView.isHidden = false
        hideToolbar(hide: true)
    }

    @IBAction func textButtonTapped(_ sender: Any) {
        editButtonStackView.isHidden = true
        isTyping = true
        let textView = UITextView(frame: CGRect(x: 0, y: canvasImageView.center.y,
                                                width: 40, height: 30))
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.textAlignment = .center
        textView.font = UIFont(name: "Helvetica", size: 30)
        textView.textColor = textColor
        textView.layer.shadowColor = UIColor.black.cgColor
        textView.layer.shadowOffset = CGSize(width: 1.0, height: 0.0)
        textView.layer.shadowOpacity = 0.2
        textView.layer.shadowRadius = 1.0
        textView.layer.backgroundColor = UIColor.black.cgColor
        textView.layer.opacity = 0.7
        textView.autocorrectionType = .no
        textView.isScrollEnabled = false
        cancelButton.isHidden = false
        doneButton.isHidden = false
        saveButton.isHidden = true
        textView.delegate = self
        canvasImageView.addSubview(textView)

        applyPlaceholderStyle(to: textView, placeholderText: textFieldPlaceholder)  // TODO: use a string constant
        let fontSize: CGSize = textFieldPlaceholder.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: "Helvetica", size: 30)!])

        textfieldLayoutWidthConstraint = NSLayoutConstraint(item: textView,
                                                            attribute: NSLayoutConstraint.Attribute.width,
                                                            relatedBy: NSLayoutConstraint.Relation.equal,
                                                            toItem: nil,
                                                            attribute: NSLayoutConstraint.Attribute.notAnAttribute,
                                                            multiplier: 1,
                                                            constant: fontSize.width + 30)
        horizontalConstraint = NSLayoutConstraint(item: textView,
                                                      attribute: NSLayoutConstraint.Attribute.centerX,
                                                      relatedBy: NSLayoutConstraint.Relation.equal,
                                                      toItem: canvasImageView,
                                                      attribute: NSLayoutConstraint.Attribute.centerX,
                                                      multiplier: 1,
                                                      constant: 0)
        verticalConstraint = NSLayoutConstraint(item: textView,
                                                    attribute: NSLayoutConstraint.Attribute.centerY,
                                                    relatedBy: NSLayoutConstraint.Relation.equal,
                                                    toItem: canvasImageView,
                                                    attribute: NSLayoutConstraint.Attribute.centerY,
                                                    multiplier: 1,
                                                    constant: 0)

        addGestures(view: textView)
        textView.becomeFirstResponder()

        canvasImageView.addConstraints([horizontalConstraint!, verticalConstraint!, textfieldLayoutWidthConstraint!])

        view.layoutIfNeeded();
        view.updateConstraints();
    }

    fileprivate func applyPlaceholderStyle(to textView: UITextView, placeholderText: String) {
        textView.textColor = UIColor.lightGray
        textView.text = placeholderText
    }

    fileprivate func applyNonplaceholderStyle(_ textView: UITextView) {
        moveCursorToStart(textView)
    }

    fileprivate func moveCursorToStart(_ textView: UITextView) {
        DispatchQueue.main.async {
            textView.selectedRange = NSMakeRange(0, 0);
        }
    }

    public func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView.text == textFieldPlaceholder {
            applyNonplaceholderStyle(textView)
        }
        return true
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
sdfas
        guard let textfieldLayoutWidthConstraint = textfieldLayoutWidthConstraint else {
            return true
        }
        if let horizontalConstraint = horizontalConstraint, let verticalConstraint = verticalConstraint {
            view.removeConstraints([horizontalConstraint, verticalConstraint])
            self.horizontalConstraint = nil
            self.verticalConstraint = nil
        }
        var returnValue = true
        let newLength = textView.text.utf16.count + text.utf16.count - range.length
        if newLength > 0 {
            if textView.text == textFieldPlaceholder {
                if text.utf16.count == 0 {  // back button
                    return false
                }
                applyNonPlaceholderStyle(to: textView)
                textView.text = ""
            }
            returnValue = true
        }
        else {
            applyPlaceholderStyle(to: textView, placeholderText: textFieldPlaceholder)
            moveCursorToStart(textView)
            returnValue = false
        }
        let strForWholeString = textView.text + text //NSString(format:"%@%@", textView.text!, text) as String
        let textSize: CGSize = strForWholeString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: "Helvetica", size: 30)!])

        let padding = CGFloat(40)
        if textSize.width + padding < UIScreen.main.bounds.width {
            textfieldLayoutWidthConstraint.constant = textSize.width + padding
        }
        return returnValue
    }

    func applyNonPlaceholderStyle(to textView: UITextView) {
        textView.textColor = textColor
    }

    @IBAction func doneButtonTapped(_ sender: Any) {
        view.endEditing(true)
        doneButton.isHidden = true
        colorPickerView.isHidden = true
        canvasImageView.isUserInteractionEnabled = true
        hideToolbar(hide: false)
        isDrawing = false
        saveButton.isHidden = false
        let img = self.canvasView.toImage()
        photoEditorDelegate?.doneEditing(image: img)
        image = img
        setImageView(image: img)
        clearButtonTapped(self)
        editButtonStackView.isHidden = false
        undoHistory.append(img)
    }

    @IBAction func saveButtonTapped(_ sender: Any) {
        editButtonStackView.isHidden = true
        let img = self.canvasView.toImage()
        photoEditorDelegate?.doneEditing(image: img)
        image = img
        setImageView(image: img)
        clearButtonTapped(self)
        photoEditorDelegate?.canceledEditing()
        editButtonStackView.isHidden = false
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: Bottom Toolbar
    
    @IBAction func shareButtonTapped(_ sender: UIButton) {
        let activity = UIActivityViewController(activityItems: [canvasView.toImage()], applicationActivities: nil)
        guard let popoverPresentationController = activity.popoverPresentationController else {
            return
        }
        popoverPresentationController.sourceRect = CGRect(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height / 2, width: 0, height: 0)
        popoverPresentationController.sourceView = self.view
        popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
        present(activity, animated: true, completion: nil)
    }
    
    @IBAction func clearButtonTapped(_ sender: AnyObject) {
        //clear drawing
        canvasImageView.image = nil
        //clear stickers and textviews
        for subview in canvasImageView.subviews {
            subview.removeFromSuperview()
        }
    }
    
    @IBAction func continueButtonPressed(_ sender: Any) {
        let img = self.canvasView.toImage()
        photoEditorDelegate?.doneEditing(image: img)
        image = img
        //self.dismiss(animated: true, completion: nil)
        setImageView(image: img)
        clearButtonTapped(self)
    }

    //MAKR: helper methods
    
    @objc func image(_ image: UIImage, withPotentialError error: NSErrorPointer, contextInfo: UnsafeRawPointer) {
        let alert = UIAlertController(title: "Image Saved", message: "Image successfully saved to Photos library", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func hideControls() {
        for control in hiddenControls {
            switch control {
            case .crop:
                cropButton.isHidden = true
            case .draw:
                drawButton.isHidden = true
            case .save:
                saveButton.isHidden = true
            case .text:
                textButton.isHidden = true
            default:
                continue
            }
        }
    }
}
